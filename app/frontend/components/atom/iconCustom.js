import Leaflet from 'leaflet'
const IconCustom = (image) =>
new Leaflet.Icon({
                iconUrl: image,
                shadowUrl: "",
                iconSize:     [39, 50], // size of the icon
                shadowSize:   [70, 15], // size of the shadow
                iconAnchor:   [20, 49], // point of the icon which will correspond to marker's location
                shadowAnchor: [0, 15],  // the same for the shadow
                popupAnchor:  [0, -55]// point from which the popup should open relative to the iconAnchor
  })
export default IconCustom;
