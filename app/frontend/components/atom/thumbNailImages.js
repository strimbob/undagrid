import React from "react"
import iconsUrls  from '../../../redux/data/icons.js'
const ThumbNailImages = (props) =>
iconsUrls.map((_img,index) =>
  <img key={index} src={_img.img}
    onClick={() => props.onClick(_img.icon,props.marker.lonLat)}
    className={`${props.className} ${_img.icon == props.marker.icon ?
    "popup__images--active":""}`}>
    </img>
)
export default ThumbNailImages
