import React from "react"
import IconCustom from '../atom/iconCustom.js';
import iconsUrls  from '../../../redux/data/icons.js'

const IconSelector = (iconName) =>{
let _icon = iconsUrls[0].img;
iconsUrls.forEach((icons,index) =>{
if(icons.icon == iconName) _icon = icons.img;
})
  return IconCustom(_icon)
}
export default IconSelector;
