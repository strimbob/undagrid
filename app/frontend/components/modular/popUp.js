import React from "react"
import {Popup } from 'react-leaflet';
import ThumbNailImages from '../atom/thumbNailImages.js'

const PopUp = (props) =>
<Popup>
  <div className={"popup"}>
    <form onSubmit={e => { e.preventDefault(); }}>
    <input className={"popup__name"} type="text" name={props.marker.name}
      placeholder={props.marker.name} autoComplete="off"
      onChange={evt => props.handleChange(evt,props.marker.lonLat)}></input>
    </form>
  <ThumbNailImages className={"popup__images"} marker={props.marker}
      onClick={props.changeIcon}/>
  <button onClick={() => props.deleteMarker(props.marker.lonLat)}
      className={"popup__button"}> Delete </button>
  </div>
</Popup>

export default PopUp;
