import React from "react"
import {  Marker } from 'react-leaflet';
import PopUp from '../modular/popUp.js'
import IconSelector from '../modular/iconSelector.js'

const Markers = (props) =>{
      if(props.markers.length != 0)
      return props.markers.map((_marker, idx) =>
          <Marker key={`marker-${idx}`} position={_marker.lonLat}
            icon={IconSelector(_marker.icon)} >
          <PopUp marker={_marker} handleChange={props.handleChange}
            deleteMarker={props.deleteMarker} changeIcon={props.changeIcon}/>
        </Marker>
      )
return null
}
export default Markers;
