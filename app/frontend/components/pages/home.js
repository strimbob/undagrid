import React from 'react';
import { connect } from "react-redux"
import { Map, TileLayer } from 'react-leaflet';
import {addMarker,delMarker,changeNameText,changeIconMaker,getCookies} from '../../../redux/2_action/markerControll.js'
import Markers from '../organisms/markers.js'

@connect((store) => {
  return {
    markerControll : store.markerControll,
  };
})

 export default class Home  extends React.Component  {
  constructor() {
    super();
  }

  componentWillMount(){
    this.props.dispatch(getCookies());
  }

  addMarker = (lonLat) => {
    this.props.dispatch(addMarker(lonLat.latlng));
  }

  deleteMarker = (lonLat) =>{
    this.props.dispatch(delMarker(lonLat));
  }

  handleChange = (name,lonLat) =>{
    const data={lonLat:lonLat,name:name.target.value};
    this.props.dispatch(changeNameText(data));
  }

  changeIcon = (icon,lonLat) =>{
    const data={lonLat:lonLat,icon:icon};
    this.props.dispatch(changeIconMaker(data))
  }

  render() {
    return (
  <Map
    className="map"
    center={[52.15955446809974, 4.493801490875807]}
    zoom={15}
    onClick={this.addMarker}>

    <TileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>

     <Markers markers={this.props.markerControll.markers}
       handleChange={this.handleChange}
       deleteMarker={this.deleteMarker}
       changeIcon={this.changeIcon}/>
  </Map>
)}}
