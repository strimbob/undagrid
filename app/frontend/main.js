import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from '../redux/1_store/store.js';
import Home from './components/pages/home.js';
const App = (
  <Provider store={store}>
    <main>
     <Home />
    </main>
  </Provider>
);
ReactDOM.render(App,document.getElementById('root'),);
