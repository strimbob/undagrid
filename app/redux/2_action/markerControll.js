export function addMarker(marker) {
  return function(dispatch ) {
    dispatch({type: "ADD--MARKER",payload: marker});
  }
}

export function delMarker(marker) {
  return function(dispatch) {
    dispatch({type: "DEL--MARKER",payload: marker});
  }
}

export function changeNameText(data) {
  return function(dispatch) {
    dispatch({type: "TEXT--MARKER",payload: data});
  }
}

export function changeIconMaker(index) {
  return function(dispatch) {
    dispatch({type: "ICON--CHANGE--MARKER",payload: index});
  }
}

export function getCookies() {
  return function(dispatch) {
    dispatch({type: "GET--COOKIES--MARKER"});
  }
}
