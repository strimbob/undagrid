import Cookies from 'js-cookie';
const  init = [{}];
init.length = 0;

export default function reducer(state={
markers: init,
  }, action) {

    const setCookies = () =>{
      Cookies.set("markers",  state.markers, { expires: 7 });
    }

    switch (action.type) {
      case "ADD--MARKER":{
        const defaultMarker = {lonLat : action.payload,name:"Name",icon:"default"}
        state.markers.push(defaultMarker)
        setCookies()
    return {...state,markers:state.markers}
  }

  case "GET--COOKIES--MARKER":{
    if(Cookies.get('markers') != undefined){
        state.markers = JSON.parse(Cookies.get('markers'));
    }
     return {...state,markers:state.markers}
  }

  case "DEL--MARKER":{
    let updatedMarkers =[];
    state.markers.forEach(m =>{
    if(action.payload != m.lonLat) updatedMarkers.push(m);})
    setCookies()
    return {...state,markers: updatedMarkers}
  }

  case "TEXT--MARKER":{
    state.markers.forEach(m =>{
      if(action.payload.lonLat == m.lonLat)
        m.name = action.payload.name
      })
      setCookies()
    return {...state,markers: state.markers}
  }
  
  case "ICON--CHANGE--MARKER":{
    state.markers.forEach(m =>{
      if(action.payload.lonLat == m.lonLat)
        m.icon = action.payload.icon
      })
    setCookies()
    return {...state,markers: state.markers}
  }
}
return state
}
