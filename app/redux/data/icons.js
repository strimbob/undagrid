const pizza  = require("../../icon/pizza.png")
const pointer  = require("../../icon/pointer.png")
const iceCream = require("../../icon/iceCream.png")

const iconsUrls =[
  {img: pointer,
  icon:"default",
  },
  {img:pizza,
  icon:"pizza",
  },
  {img:iceCream,
  icon:"iceCream",
  },
];

export default iconsUrls;
