import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
const rootPath = path.join(__dirname, './');
const appPath = path.join(rootPath, 'app');
module.exports = {
    devtool: 'cheap-module-eval-source-map',
  entry: [path.join(appPath, 'frontend/main.js'), path.join(appPath, 'frontend/scss/main.scss')],
  output: {
    path: rootPath,
    filename: './dist/bundle.js',
  },
  devServer: {
    port: 8080,
    contentBase: rootPath,
  },
  module: {
    rules: [
      {
        test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        exclude: /components/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!sass-loader',
        }),
      },
      {test: /\.(png|jpg|gif)$/,
        use: [{
          loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'image/png',
              name: 'images/[name].[ext]',
            }
          }
        ],
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('./dist/styles.css'),
    new HtmlWebpackPlugin({ template: path.join(appPath, 'html/index.html') }),
  ],
};
